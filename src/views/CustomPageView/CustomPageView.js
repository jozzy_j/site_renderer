import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import {fetchPosts, receivePosts } from '../../redux/actions';
import * as ComponentCollection from 'allComponents/ComponentCollection.js';

export class CustomPageView extends React.Component {
  componentDidMount () {
    const {location, dispatch} = this.props;
    let path = location['pathname'];
    if (path === '/') {
      path = '/index';
    }
    dispatch(fetchPosts(path));
  }

  render () {
    let {dispatch, selectedComponents, isPagesAvailable} = this.props;
    let getComponents = [];
    if (isPagesAvailable === false) {
      getComponents.push(
        <div className='container text-center'>
          <h1 style={{position: 'relative', color: '#E61E50', top: '200px'}}>
            No index to show. Please publish index page  with url '/index' from Page Maker.
          </h1>
        </div>
      );
    } else {
      let components = selectedComponents;
      console.log('comp: ',selectedComponents );
      for (var key in components) {
        if (components[key].parent === '') {
          let final = components[key].final;
          let compName = key.split('_')[0];
          let Component = ComponentCollection[compName];
          let currrentKey = key;
          getComponents.push(
            <Component 
              key={key.split('_')[1]} {...components[key].data.properties}
              selectedComponents={selectedComponents}
              dispatch={dispatch}
              final={final}/>
          );
        }
      }
    }
    return (
        <div>
          {getComponents}
        </div>
    );
  }
}

const mapStateToProps = (state) => ({
  selectedComponents: state.selectedComponents,
  isPagesAvailable: state.checkPages
});
export default connect(mapStateToProps)(CustomPageView);