import React from 'react';
import * as components from './InfoPageComponents';
import ComponentsData from 'allComponents/ComponentsData';
import { connect } from 'react-redux';
import {setEdit, removeComponent, dropComponent} from 'redux/actions';
import DroppableWrapper from 'containers/wrappers/DroppableWrapper';
import EditableWrapper from 'containers/wrappers/EditableWrapper';
import {Grid, Row, Col} from 'react-bootstrap';

var final = '';
var count = 0;
class InfoPageTemplate extends React.Component {
  static propTypes = {
    dispatch: React.PropTypes.func,
    selectedComponents: React.PropTypes.object
  };
  handleDelete = (currrentKey) => {
    let { selectedComponents, dispatch } = this.props;
    Object.keys(selectedComponents).forEach((key) => {
      if (selectedComponents[key].parent.split('_')[0] === currrentKey.split('_')[0]) {
        dispatch(removeComponent(key));
      }
    });
  };

  componentWillMount () {
    let {dispatch, selectedComponents} = this.props;

    // checking page is rendering in editor or Renderer by tempFlag
    let tempFlag = false;
    for (let key in selectedComponents) {
      if (selectedComponents[key].final === true) {
        tempFlag = true;
        break;
      }
    }

    // Auto dropping components for this template when page is in editor
    if (tempFlag === false) {
      var compData = {properties: {}};
      for (let key in ComponentsData['PageHeader'].properties) {
        compData.properties[key] = ComponentsData['PageHeader'].properties[key];
      }
      dispatch(dropComponent(
        'PageHeader',
        compData,
        this.constructor.displayName
      ));

      compData = {properties: {}};
      for (let key in ComponentsData['NavBar'].properties) {
        compData.properties[key] = ComponentsData['NavBar'].properties[key];
      }
      dispatch(dropComponent(
        'NavBar',
        compData,
        this.constructor.displayName
      ));

      compData = {properties: {}};
      for (let key in ComponentsData['CustomLink'].properties) {
        compData.properties[key] = ComponentsData['CustomLink'].properties[key];
      }
      for (var i=0;i<20;i++) {
        dispatch(dropComponent(
        'CustomLink',
        compData,
        this.constructor.displayName
        ));
      }
 
      compData = {properties: {}};
      for (let key in ComponentsData['Header'].properties) {
        compData.properties[key] = ComponentsData['Header'].properties[key];
      }
      for (var j=0;j<3;j++) {
       dispatch(dropComponent(
         'Header',
         compData,
         this.constructor.displayName
       ));
      }

      compData = {properties: {}};
      for (let key in ComponentsData['Paragraph'].properties) {
        compData.properties[key] = ComponentsData['Paragraph'].properties[key];
      }
      dispatch(dropComponent(
        'Paragraph',
        compData,
        this.constructor.displayName
      ));

      compData = {properties: {}};
      for (let key in ComponentsData['ItemDescription'].properties) {
        compData.properties[key] = ComponentsData['ItemDescription'].properties[key];
      }
      for (var k=0;k<4;k++) {
        dispatch(dropComponent(
          'ItemDescription',
          compData,
          this.constructor.displayName
        ));
      }

      compData = {properties: {}};
      for (let key in ComponentsData['PageFooter'].properties) {
        compData.properties[key] = ComponentsData['PageFooter'].properties[key];
      }
      dispatch(dropComponent(
        'PageFooter',
        compData,
        this.constructor.displayName
      ));

    //  dispatch(setEdit());
    }
  }

  render () {
    
    let { selectedComponents, dispatch } = this.props;
    
    // variables to hold components
    let PageHeader = '';
    let NavBar = '';
    let PageFooter = '';
    let menuLinks = [];
    let topicLinks1 = [];
    let topicLinks2 = [];
    let Headers = [];
    let Paragraph = '';
    let ItemDescriptions = [];
    

    // Loop for rendering components from state
    Object.keys(selectedComponents).forEach((key) => {
      if (selectedComponents[key].parent === 'InfoPageTemplate') {
        final = selectedComponents[key].final;
        let compName = key.split('_')[0];
        let Component = components[compName];
        let currrentKey = key;
        let CompWithProps = '';
        if (selectedComponents[key].final === false) {
          CompWithProps = (
              <EditableWrapper
              key={key.split('_')[1]}
              onClick={(ev) => { ev.stopPropagation(); dispatch(setEdit(currrentKey)); }}
              onDelete={() => dispatch(removeComponent(currrentKey))}
              isEditable={selectedComponents[key].edit}
              template={true} >
                <div style={{minHeight: compName === 'Paragraph' ?'50px' : 'none'}}>
                  <Component {...selectedComponents[key].data.properties}
                  selectedComponents={selectedComponents}
                  dispatch={dispatch}/>
                </div>
              </EditableWrapper>
          );
        } else {
          CompWithProps = (
            <Component {...selectedComponents[key].data.properties}
            selectedComponents={selectedComponents}
            dispatch={dispatch}/>
          );
        }
        
        if (compName === 'CustomLink') {
          if (menuLinks.length < 10)
            menuLinks.push(CompWithProps)
          else if (topicLinks1.length < 5)
            topicLinks1.push(CompWithProps)
          else
            topicLinks2.push(CompWithProps)
        } else if (compName === 'PageHeader') {
          PageHeader = CompWithProps;
        } else if (compName === 'NavBar') {
          NavBar = CompWithProps;
        } else if (compName === 'Header') {
          Headers.push(CompWithProps);
        } else if (compName === 'Paragraph') {
          Paragraph = CompWithProps;
        } else if (compName === 'PageFooter') {
          PageFooter = CompWithProps;
        } else if (compName === 'ItemDescription') {
          ItemDescriptions.push(CompWithProps);
        }
      }
    });

    return (
      <div>
        <div id='PageHeader'>
          {PageHeader}
        </div>
        <div id='NavBar' style={{border: final ? 'none' : '1px solid #DAD4D4'}}>
          {NavBar}
        </div>

        <Grid style={{width: '100%'}}>
          <Row>
          <Col lg={3} md={3} sm={3} 
          style={{border: '2px solid #E61E50', borderRadius: '5px', backgroundColor: '#F1F0F0'}}>
            {menuLinks.map((link, i) => {
              return (
                <div id={'link-'+i} key={i} style={{border: final ? 'none' : '1px solid #DAD4D4', marginTop: '5px'}}>
                  {link}
                </div>
              )
            })}
          </Col>

          <Col lg={9} md={9} sm={9}>
            <Row>
            <div id='Header' style={{border: final ? 'none' : '1px solid #DAD4D4'}}>
              {Headers.map((hdrs, i) => {
                if(i == 0) {
                  return <div key={i}>{hdrs}</div>
                }
              })}
            </div>
            <div id='Paragraph' style={{border: final ? 'none' : '1px solid #DAD4D4'}}>
              {Paragraph}
            </div>
          </Row>
          <Row>
            <Col lg={6} md={6} sm={6} >
                {Headers.map((hdrs, i) => {
                if(i == 1) {
                  return <div key={i} style={{border: final ? 'none' : '1px solid #DAD4D4'}}>{hdrs}</div>
                }
              })}
                {topicLinks1.map((link, i) => {
                  return (
                    <div id={'link-'+i} key={i} style={{border: final ? 'none' : '1px solid #DAD4D4', marginTop: '5px'}}>
                      {link}
                    </div>
                  )
                })}
              </Col>

              <Col lg={6} md={6} sm={6}>
              {Headers.map((hdrs, i) => {
                if(i == 2) {
                  return <div key={i} style={{border: final ? 'none' : '1px solid #DAD4D4'}}>{hdrs}</div>
                }
              })}
                {topicLinks2.map((link, i) => {
                  return (
                    <div id={'link-'+i} key={i} style={{border: final ? 'none' : '1px solid #DAD4D4', marginTop: '5px'}}>
                      {link}
                    </div>
                  )
                })}
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        {ItemDescriptions.map((item, i) => {
          return (
            <Col lg={3} md={3} sm={3}>
            <div>{item}</div>
          </Col>
          );
        })}
      </Row>
    </Grid>
        <div id='PageFooter'>
          {PageFooter}
        </div>
      </div>
    );
  }
}

export default InfoPageTemplate
