import React from 'react';

var deleteButton = {
    position: 'relative',
    marginLeft: '97%',
    zIndex: '1',
    top: '-5px',
    borderRadius: '50%',
    backgroundColor: '#EC3737',
    border: '1px solid #EC3737'
}

export default class EditableWrapper extends React.Component {
  static propTypes = {
    onDelete: React.PropTypes.func.isRequired,
    onClick: React.PropTypes.func.isRequired,
    isEditable: React.PropTypes.bool.isRequired,
  };

  render() {
  	let {onClick, isEditable, children, onDelete, template} = this.props;
    let templateFlag = template ? true : false;
    return (
      <div>
        {!templateFlag && isEditable && <button style={deleteButton} onClick={() => onDelete()}>x</button>}
        <div onClick={(e) => onClick(e)} style={{border: isEditable ? '1px solid green' : 'none',marginTop: templateFlag ? 0 :(isEditable ? '-24px' : 0)}} >
        	{children}
        </div>
      </div>
    );
  }
}
