import React from 'react';
import {setEdit, dropComponent} from 'redux/actions';
import { connect } from 'react-redux';
import ComponentsData from 'allComponents/ComponentsData';

class DroppableWrapper extends React.Component {
  static propTypes = {
    name: React.PropTypes.string
  };

  drop = (ev, componentName, components, targetId) => {
    let {dispatch} = this.props;
    ev.preventDefault();
    ev.stopPropagation();
    var compName = ev.dataTransfer.getData('name');
    let flag = false;
    if (components !== undefined) {
      Object.keys(components).forEach((key) => {
        if(key === compName) {
            flag = true;
        }
      });
    }

    if (flag) {
      const {dispatch} = this.props;
    	var compData = {properties: {}};
	    for (var key in ComponentsData[compName].properties) {
	      compData.properties[key] = ComponentsData[compName].properties[key];
	    }
    	// var compData = Object.assign({},ComponentsData[compName]);
  		let parent = componentName + '_' + targetId;
	    dispatch(setEdit());
	    dispatch(dropComponent(compName,compData,parent));
    } else {
      alert("you can't drop " + compName + " here");
    }
  };

  allowDrop = (ev) => {
    ev.preventDefault();
  };

  render () {
  	var childrenWithProps = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, { drop:this.drop , allowDrop: this.allowDrop});
    });
    return (
      <div>{childrenWithProps}</div>
    );
  }
}

/*const mapStateToProps = (state) => ({
  selectedComponents: state.selectedComponents
});
export default connect(mapStateToProps)(DroppableWrapper);
*/
export default DroppableWrapper
