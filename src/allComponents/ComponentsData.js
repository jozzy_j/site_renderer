
import deepfreeze from 'deep-freeze';

const ComponentData = {
  PageHeader: {
    properties: {
      file: 'sigmaImg'
  	}
  },
  ItemDescription: {
  	properties: {
  	  file: 'defaultImage',
  	  itemName: 'sigma',
  	  itemDetails: 'description'
  	}
  },
  PageFooter: {
    properties: {
      text: 'Sigma-Aldrich@2016'
    }
  },
  ProductPanelContainer: {
    properties: {
      title: ['title1','title2','title3','title4','title4','title6'],
      tabsCount: 6
    }
  },
GridLayout:{
    properties:{
    },
    layouts:{
    }
  },
  VerticalLayout: {
    properties: {}
  },
  HorizontalLayout: {
    properties: {}
  },
  HomePageTemplate: {
    properties: {}
  },
  CarouselInstance: {
    properties: {
      initialState: {
      file: 'defaultImage',
      contentTitle: 'Title of slide',
      content: 'Slide content'},
      items: [],
      direction: 'left'
    }
  },
  NavBar: {
    properties: {
    initialState: {
      title: 'title',
      options: 'Options'},
      listItems: {}
    }
  },
  CustomLink: {
    properties: {
      text: 'Add text here',
      linkValue: 'Add the target of link here'
    }
  },
  CustomImage: {
    properties: {
      file: 'city',
      name: 'Image',
      linkValue: '#'
    }
  },
  Paragraph: {
    properties: {
      content: 'add some content'
    }
  },
  Header: {
    properties: {
      content: 'Give Header'
    }
  },
  InfoPageTemplate: {
    properties: {
    }
  }
};



deepfreeze(ComponentData);
export default ComponentData