//components
export ProductPanelContainer from 'components/ProductPanelContainer/ProductPanelContainer';
export CarouselInstance from 'components/Carousel';
export NavBar from 'components/NavBar';
export CustomImage from 'components/CustomImage';
export CustomLink from 'components/CustomLink';
export PageHeader from 'components/PageHeader';
export PageFooter from 'components/PageFooter';
export ItemDescription from 'components/ItemDescription';
export Header from 'components/Header';
export Paragraph from 'components/Paragraph';

//layouts
export GridLayout from 'layouts/GridLayout';

//templates
export HomePageTemplate from 'containers/templates/HomePageTemplate/HomePageTemplate';
export InfoPageTemplate from 'containers/templates/InfoPageTemplate/InfoPageTemplate';
