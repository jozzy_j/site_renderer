export default (state = true, action) => {
  switch (action.type) {
    case 'PAGES_IS_AVAILABLE':
      return false;
    default:
      return state;
  }
};