export default (state = {}, action) => {
  switch (action.type) {
    case 'RECEIVE_POSTS':
      return Object.assign({},state,action.posts);
    default:
      return state;
  }
};