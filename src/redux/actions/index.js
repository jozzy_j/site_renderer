function receivePosts(json) {
  return {
    type: 'RECEIVE_POSTS',
    posts: json
  }
}

function pagesIsAvailable () {
  return {
    type: 'PAGES_IS_AVAILABLE'
  }
}

export function fetchPosts(path) {
  return dispatch => {
    fetch('http://localhost:1337/name?pageUrl='+path).then((response) => {
      return response.json();
    }).then((json) => {
      console.log('data', json);
      dispatch(receivePosts(json[0].data));
    }).catch((ex) => {
      console.log('parsing failed', ex);
      dispatch(pagesIsAvailable());
    })
  }
}