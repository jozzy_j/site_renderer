import { combineReducers } from 'redux'
import { routeReducer as router } from 'react-router-redux'
import selectedComponents from './modules/SelectedComponent';
import checkPages from './modules/checkPages';

export default combineReducers({
  selectedComponents,
  checkPages,
  router
})
