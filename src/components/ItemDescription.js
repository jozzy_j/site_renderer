import React from 'react';
import {Thumbnail} from 'react-bootstrap';
import images from 'img/images';

export default class ItemDescription extends React.Component {
  static propTypes = {
    itemName: React.PropTypes.string,
    itemDetails: React.PropTypes.string,
  };

  render () {
    let { file, itemName, itemDetails, style} = this.props;
    if(file=="defaultImage" || file == ""){
      let image = images[file];
      return(
      <div style={style !== undefined ? style : {}}>
        <Thumbnail src={images.defaultImage} alt='Item'>
          <h3>{itemName}</h3>
          <p>{itemDetails}</p>
        </Thumbnail>
      </div>
    );
    }
    else{
    let src=file.split(/\\/);
    var src1=src[2];
    let src2=src1.split(/\./);
    let src3=src2[0];
    let image = images[src3];
    return (
      <div style={style !== undefined ? style : {}}>
        <Thumbnail src={image} alt='Item'>
          <h3>{itemName}</h3>
          <p>{itemDetails}</p>
        </Thumbnail>
      </div>
    );
  }
  }
}
