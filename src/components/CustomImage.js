import React from 'react';
import {Image} from 'react-bootstrap';
import images from 'img/images';

export default class CustomImage extends React.Component {
  static propTypes = {
    file: React.PropTypes.string,
    name: React.PropTypes.string,
    linkValue: React.PropTypes.string
  };
  render () {
    let {file, name, linkValue} = this.props;
    if (file === 'city' || file === '' ) {
      return (
        <div>
          <a href={linkValue} >
            <Image src={images.city} alt={linkValue} name={name} responsive />
          </a>
        </div>
            );
    }
    else {
      let src = file.split(/\\/);
      let src1 = src[2];
      let src2 = src1.split(/\./);
      let src3 = src2[0];
      let imageSrc = images[src3];
      return (
        <div>
          <a href={linkValue}>
            <Image src={imageSrc} alt={linkValue} name={name} responsive />
          </a>
        </div>
        );
    }
  }
}
