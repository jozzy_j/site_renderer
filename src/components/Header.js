import React from 'react';

export default class Header extends React.Component {
  static propTypes = {
    content: React.PropTypes.string,
    fontSize: React.PropTypes.string
  };

  constructor(props) {
    super(props);
  }
  render() {
    let {content,fontSize,color} = this.props;
    let header;
    return (
      <p style={{marginTop: 0, marginBottom: 0, color:color,fontSize:fontSize}}>{content}</p>
    );
  }
}
