import React from 'react';
import { Tabs, Tab, Grid, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as components from './ProductPanelComponents';
import {setEdit, removeComponent, saveLS} from 'redux/actions';
import EditableWrapper from 'containers/wrappers/EditableWrapper';
import DroppableWrapper from 'containers/wrappers/DroppableWrapper';
import {WidthProvider, Responsive }  from 'react-grid-layout';
const ResponsiveReactGridLayout = WidthProvider(Responsive);

var tabStyle = {
  grid: {
    position: 'absolute',
    minHeight:'-webkit-calc(100% - 60px)',
    width: '100%',
    top: '60px'
  },
  normal: {
    minHeight:'300px'
  }
}

var deleteButton = {
    position: 'absolute',
    right: '-3px',
    zIndex: '1',
    top: '-6px',
    borderRadius: '50%',
    backgroundColor: '#EC3737',
    border: '1px solid #EC3737'
}

let CurrentComponent = '';
class ProductPanelContainer extends React.Component {
  static propTypes = {
    selectedComponents: React.PropTypes.object,
    dispatch: React.PropTypes.func,
    title: React.PropTypes.array(React.PropTypes.string),
    tabsCount: React.PropTypes.num,
    grid: React.PropTypes.bool
  };

  static defaultProps = {
    className: "layout",
    cols: {lg: 24, md: 24, sm: 24, xs: 24, xxs: 24},
    rowHeight: 30
  };

  onLayoutChange = (layout, layouts) => {
    if (this.props.final === false) {
      let { dispatch } = this.props;
      Object.keys(layouts).forEach((key) => {
        if (key === 'sm') {
          layouts.lg = layouts[key];
        }
      });
      if (CurrentComponent !== '') {
        dispatch(saveLS(this.constructor.displayName, layouts));
      }
      // this.setState({layouts});
    }
  };

  render() {
    let {title, tabsCount, style, grid, dispatch, selectedComponents, final} = this.props;

    // setting this component's style, when it is inside grid or when not
    let tabsStyle = grid ? tabStyle.grid : tabStyle.normal;

    // array containing tabs titles
    let titles = [];

    // checking title from props is string or not
    if (typeof title === 'string') {
      titles = title.split(',');
      
    } else {
      titles = title
    }

    // creating object of title for tabs 
    let tabTitle = {};
    /*for (let i=0; i<tabsCount; i++) {
      tabTitle[`tab${i+1}`] = (<h4>{titles[i]}</h4>);
      console.log("inside",titles[i])
    }*/
    titles.forEach((titl, i) => {
      tabTitle[`tab${i+1}`] = (<h4>{titl}</h4>);
      console.log("inside",tabTitle)
    });
 
    // creating object of tabs for accessing droped components 
    let getComponents = {};
    titles.forEach((titl, i) => {
      getComponents[`tab${i+1}`] = [];
    });

    let layouts = {};
    Object.keys(selectedComponents).forEach((key) => {
      if (key.split('_')[0] === 'ProductPanelContainer') {
        layouts = selectedComponents[key].data.layouts;
        CurrentComponent = key;
      }
      console.log("check final:", final);
    });

    let componentName = this.constructor.displayName;

    // loop for rendering droped components
    Object.keys(selectedComponents).forEach((key) => {
      let compName = key.split('_')[0];
      let Component = components[compName];
      if(Component != undefined)
      {
        
        let currentKey = key;
        let componentToRender = '';
        if (selectedComponents[key].final === false) {
          componentToRender = (
            <div 
              _grid={{w: 2, h: 3, x: 0, y: 0}} 
              key={key.split('_')[1]}
              onClick={(ev) => { ev.stopPropagation(); dispatch(setEdit(currentKey)); }}
              style={{
                border: selectedComponents[key].edit ? '1px solid green' : '1px solid #DAD4D4'
              }}>
                <div>
                  {selectedComponents[key].edit && 
                    <button 
                    style={deleteButton} 
                    onClick={() => {dispatch(removeComponent(currentKey)); this.handleDelete(currentKey);}}>
                      x
                    </button>
                  }
                  <DroppableWrapper>
                    <Component {...selectedComponents[key].data.properties}
                    selectedComponents={selectedComponents}
                    dispatch={dispatch}
                    grid={true}/>
                  </DroppableWrapper>
                </div>
              </div>
          );
        } else {
          componentToRender = (
            <div _grid={{w: 2, h: 3, x: 0, y: 0}} key={key.split('_')[1]}>
                <Component {...selectedComponents[key].data.properties}
                  selectedComponents={selectedComponents}
                  dispatch={dispatch}
                  key={key.split('_')[1]}
                  grid={true}/>
            </div>
          );
        }

        titles.forEach((ttl, i) => {
          if (selectedComponents[key].parent.split('_')[1] === (`tab${i+1}`)) {
            getComponents[`tab${i+1}`].push(componentToRender);
          }
        });
      }
    });

    let getTabs = [];
    for (var i=0;i<tabsCount;i++) {
      let curIndex = i;
      getTabs.push(
        <Tab eventKey={curIndex + 1} title={tabTitle[`tab${curIndex+1}`]}
        style={tabsStyle}
        key={curIndex+1}>
          <div
          id={`tab${curIndex+1}`}
          onDrop={(e) => this.props.drop(e, this.constructor.displayName, components, `tab${curIndex+1}`)} 
          onDragOver={this.props.allowDrop}
          style={{
            position: grid ? 'absolute' : 'none',
            minHeight: grid ? '100%' : '300px',
            width: grid ? '100%' : 'none'
          }}>
            <ResponsiveReactGridLayout
            ref="rrgl"
            {...this.props}
            layouts={layouts}
            onLayoutChange={this.onLayoutChange}
            isDraggable={final ? false : true}
            isResizable={final ? false : true}>
              {getComponents[`tab${curIndex+1}`]}
            </ResponsiveReactGridLayout>
          </div>
        </Tab>
      )
    }

    return (
      <div style={style !== undefined ? style : {}}>
        <Tabs defaultActiveKey={1}>
          {getTabs}
        </Tabs>
      </div>
    );
  }
}


/*const mapStateToProps = (state) => ({
  selectedComponents: state.selectedComponents
});
export default connect(mapStateToProps)(ProductPanelContainer);*/
export default ProductPanelContainer
