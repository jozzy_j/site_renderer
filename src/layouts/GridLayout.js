import React from 'react';
import * as components from './GridLayoutComponents';
import {setEdit, removeComponent, saveLS} from 'redux/actions';
import DroppableWrapper from 'containers/wrappers/DroppableWrapper';
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
import {WidthProvider, Responsive }  from 'react-grid-layout';
const ResponsiveReactGridLayout = WidthProvider(Responsive);

// const originalLayouts = getFromLS('layouts') || {};

var style = {
  border: '1px solid #DAD4D4',
  minHeight: '200px',
  backgroundColor: 'white',
  overflow: 'hidden'
};

var deleteButton = {
    position: 'absolute',
    right: '-3px',
    zIndex: '1',
    top: '-6px',
    borderRadius: '50%',
    backgroundColor: '#EC3737',
    border: '1px solid #EC3737'
}

let CurrentComponent = '';
class GridLayout extends React.Component {

  static defaultProps = {
    className: "layout",
    cols: {lg: 24, md: 24, sm: 24, xs: 24, xxs: 24},
    rowHeight: 30
  };

  constructor (props) {
    super(props);
    this.state = {
      layouts: {},
      final: false
    };
  }

  componentWillMount () {
    let { selectedComponents, dispatch } = this.props;
    Object.keys(selectedComponents).forEach((key) => {
      if (key.split('_')[0] === 'GridLayout') {
        this.setState({layouts: selectedComponents[key].data.layouts,
                        final: selectedComponents[key].final});
      }
    });
  }

   handleDelete = (currrentKey) => {
    let { selectedComponents, dispatch } = this.props;
    Object.keys(selectedComponents).forEach((key) => {
      if (selectedComponents[key].parent.split('_')[0] === currrentKey.split('_')[0]) {
        dispatch(removeComponent(key));
      }
    });
  };


  onLayoutChange = (layout, layouts) => {
    if (this.state.final === false) {
      let { dispatch } = this.props;
      Object.keys(layouts).forEach((key) => {
        if (key === 'sm') {
          layouts.lg = layouts[key];
        }
      });
      if (CurrentComponent !== '') {
        dispatch(saveLS(CurrentComponent, layouts));
      }
      this.setState({layouts});
    }
  };

  render () {
    let {dispatch, selectedComponents} = this.props;
    let getComponents = [];
    // setting bottom margin in edit mode,
    // and calculating parent id
    /*Object.keys(selectedComponents).forEach((key) => {
      if (selectedComponents[key].parent.split('_')[0] === 'GridLayout') {
        childCount++;
        style.width = (100/childCount) + '%';
      }
    });*/
    // loop for rendering droped components
    Object.keys(selectedComponents).forEach((key,i) => {
      let compName = key.split('_')[0];
      // console.log('key before:',compName);
      let Component = components[compName];
      if (Component !== undefined) {
        let currentKey = key;
        if (selectedComponents[key].parent.split('_')[0] === 'GridLayout') {
          CurrentComponent = key;
          if (selectedComponents[key].final === false) {
            // console.log('key :',key);
            getComponents.push(
              <div 
              _grid={{w: 2, h: 3, x: 0, y: 0}} 
              key={key.split('_')[1]}
              onClick={(ev) => { ev.stopPropagation(); dispatch(setEdit(currentKey)); }}
              style={{
                border: selectedComponents[key].edit ? '1px solid green' : '1px solid #DAD4D4'
              }}>
                <div>
                  {selectedComponents[key].edit && 
                    <button 
                    style={deleteButton} 
                    onClick={() => {dispatch(removeComponent(currentKey)); this.handleDelete(currentKey);}}>
                      x
                    </button>
                  }
                  <DroppableWrapper 
                  selectedComponents={selectedComponents}
                  dispatch={dispatch}>
                    <Component {...selectedComponents[key].data.properties}
                    selectedComponents={selectedComponents}
                    dispatch={dispatch}
                    grid={true}/>
                  </DroppableWrapper>
                </div>
              </div>
            );
          } else {
            style.border = 'none';
            getComponents.push(
              <div _grid={{w: 2, h: 3, x: 0, y: 0}} key={key.split('_')[1]}>
                <Component {...selectedComponents[key].data.properties}
                  selectedComponents={selectedComponents}
                  dispatch={dispatch}
                  key={key.split('_')[1]}
                  grid={true}/>
              </div>
            );
          }
        }
      }
    });
    return (
      <div
      style={style}
      onDrop={(e) => this.props.drop(e, this.constructor.displayName, components)}
      onDragOver={this.props.allowDrop}>
        <ResponsiveReactGridLayout
        ref="rrgl"
        {...this.props}
        layouts={this.state.layouts}
        onLayoutChange={this.onLayoutChange}
        isDraggable={this.state.final ? false : true}
        isResizable={this.state.final ? false : true}>
          {getComponents}
        </ResponsiveReactGridLayout>
      </div>
    );
  }
}

export default GridLayout;
